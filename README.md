WAKU WAKU iOS SDK
====================

This iOS library is a facility to have a game get coupons from the WAKU WAKU service.

Usage
-----

Using the SDK in your application consists of:

* Adding the SDK to the Xcode project.
* Activating the SDK in the application.
* Querying for coupons at the best time in your user experience.
* Displaying coupons and managing user actions.

### Add the SDK to your Xcode project

The SDK is a single bundle named `WakuWakuSDK.framework`. You can download the latest version and future updates on our [hosted repositories](https://bitbucket.org/bic/wakuwaku-ios/downloads). The downloads we offer are ZIP archives. Once you have unzipped the archive, the framework file is available to add to the project.

In Xcode, there are several ways to add a framework. Here is one:

* Click your project name in the navigator (left-hand hierarchy).
* Select the Build Phases tab.
* Unfold Link Binary With Libraries
* Drag the `WakuWakuSDK.framework` file in the list of linked libraries.

### Construction and Activation of the SDK

You can create an instance of the SDK using the constructor:

    + (WakuSDK *) activate:(NSString *)myCompanyId
                     appId:(NSString *)myAppId
               orientation:(NSString *)desiredOrientation
           okToAskLocation:(BOOL)canLocate
      okToEstimateLocation:(BOOL)canEstimate
            productionMode:(BOOL)mode;

This class method provides with an SDK instance. Further calls to this method return
  the same instance.

The SDK is an active subsystem for your application. Please note its activities:
* It makes some network calls to preload coupons.
* It performs some cryptographic operations (encryption).
* It manages the lifecycle of coupons, and it automatically discards coupons that cannot be used, and download new ones as needed.

Parameters:
* myCompanyId is a unique identifier provided by WAKU WAKU to identify the company using the SDK.
* myAppId is a unique identifier provided by WAKU WAKU to identify the application using the SDK.
* desiredOrientation specifies the type of coupon requested by the SDK. Currently, either "landscape" or "portrait" are supported.
* okToAskLocation specifies whether the SDK is allowed to ask the user for its location. Location is used solely to get coupons with location requirements. E.g. a restaurant chain Location is used solely to get coupons with location requirements. E.g. a restaurant chain
* okToEstimateLocation specifies whether the SDK can estimate the user location to serve localized coupons based on a rough guess of the position (based on the IP address). Estimates are made and updated at most every hour. This is a fallback mechanism to a more reliable location available with the okToAskLocation parameter. The intent of this parameter is the same as okToAskLocation, and a fallback solution for older devices.
* productionMode specifies the backend used by the SDK. YES uses the production backend, with real coupons. NO uses the test server with fake coupons and loose usability for demonstrations and development.

Return value: The returned value is a WakuSDK object pointer, or nil if either:
* The country is not supported by the WAKU WAKU service.
* The coupon server is unreachable due to network issues.
* An error occurred, such as an allocation issue.

In all error cases the SDK does not consume runtime resources.
 
### Request a coupon

The SDK provides two ways to access coupons: Asynchronous (using delegates) and synchronous (blocking). The choice depends on your application requirements, but the asynchronous approach is recommended for mobile applications.

#### Asynchronous requests

    - (void) requestCoupon:(id<WakuCouponStateProtocol>)delegate;

This method is a request for coupon. Any coupon received by the SDK can be collected by a delegate implementing the `couponReady:` and `noCouponThisTime:` callbacks from the WakuCouponStateProtocol protocol.

Note this is a *request* and two situations can happen as a result:
* A coupon is available and presented to the delegate via the `couponReady:` callback.
* No coupon is available at the time of the request (typically, no coupon matching the application or advertisers configurations). The delegate is then notified with the `noCouponThisTime:` callback.

Parameter:
* delegate is an object complying with the WakuCouponStateProtocol protocol, notably the `couponReady:` and `noCouponThisTime:` notifications.

#### Synchronous requests

    - (WakuCoupon *) getCoupon;

This gets a coupon right away, if available.

If no coupon is available at some point in time, the SDK will automatically collect more. Please be aware that network latency and CPU load affect when collection completes. This API is really a "best effort", and it will return a coupon only if it has been completely downloaded (image data and required metadata).

### Displaying coupons

Coupons are basically images provided by the SDK, and can be used as such. The SDK does not provide default display at this point, but it will be available soon.

For custom display, please keep in mind that our common goal is to offer users appealing products in the best UX possible. We found that full-screen displays of coupon are appealing, together with sufficiently large controls to collect the email address for sending the associated gift.

### Managing user actions

The SDK provides several action listeners. At this point, the most relevant one is sending gifts by email. This can be done asynchronously or synchronously, depending on your application requirements.

#### Asyncrhonous email sending

    - (void) postCouponByEmail:(WakuCoupon *)coupon
                         email:(NSString *)email
                      delegate:(id<WakuCouponStateProtocol>)delegate;

This orders WAKU WAKU to deliver the coupon by email. After this call, two situations can happen:
* The coupon is delivered successfully to the user's input email address. The delegate (usually the host application) is notified with `emailSent:` in the `WakuCouponStateProtocol` protocol.
* The coupon delivery fails for some reason. The delegate is notified with `emailSendingFailure:`, including an error description.

Parameters:
* coupon is the coupon object referring to the coupon that should be sent to the user.
* email is the address to send the coupon to.
* delegate is an object complying with the WakuCouponStateProtocol protocol, notably the `emailSent:` and `emailSendingFailure:` notifications.

#### Syncrhonous email sending

    - (BOOL) sendCouponByEmail:(WakuCoupon *)coupon
                         email:(NSString *)email;

This enables to send an email and get a confirmation as a result.

Return value: YES if the email has been sent successfully; NO if there was a known problem (e.g. no network).

Important: This method does not check whether the email is correctly formatted, and it just performs a network operation. So YES does not guarantee the email reached someone. It just guarantees that the coupon has been successfully passed to the SMTP server.

Location Usage
--------------

The SDK can use the user location to request for localized gifts, like a free drink at a restaurant in the city where the user is.

The host application has full control of the location usage. The SDK activation API defaults to NO location usage, and the host application can decide to let the SDK use location too (see the `activate` methods).

When the SDK is authorized by the application to use location information, the SDK just registers to iOS significant-change location service, which is today the most energy-efficient, and provides rough location data both sufficient for the SDK and respectful of the user's privacy. The SDK does not keep track of the user location. Rough location information (city level) is just added to coupon requests to get localized gifts, and then discarded. Server requests are anonymous (see the request parameter contents to confirm), thus, we hope, reducing any worry about what can be done with any location data point.

The host application may already be using location data, and still prevent the SDK from using it. The host application really has full control about the SDK behaviour related to the use of the iOS location services.

We recommend host applications who want to use location information to follow Apple's advice:

> Another way to build trust is to include the NSLocationUsageDescription key in your app’s Info.plist file and set the value of that key to a string that describes how your app intends to use location data.


What it does
------------

This SDK fetches a coupon in the background, ready to get displayed when the player has achieved the target you have chosen in the game (e.g. complete a level, acquired some object, collected X gems/coins). When the achievement is completed, the SDK displays a modal view that shows the coupon, and allows to send it by email, or to discard it. The email needs be entered only once.

The view also shows terms of use, a WAKU button that links to the service home page, and a "W" button for the upcoming Wallet application, for coupon management.

Coupon or not Coupon?
---------------------

Depending on many conditions defined by advertisers, availability, location, user demographics, and yourself, the request MAY or MAY NOT return a coupon. This call request code handles the cases as follows:

* If a coupon is available, it is shown in a dialog.
* If no coupon is available, the call is ignored and the flow returns to the caller.

Call requests are independentent, so if the user has received no coupon at some point (and for some varying reasons), she may well receive it at the next call.

Note that you control when the call is made, as you please.

